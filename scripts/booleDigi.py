from Configurables import Boole, LHCbApp, L0Conf, CondDB
from Boole.Configuration import *
import pdb as mypdb
B = Boole()


#Tags for 2016
#LHCbApp().DDDBtag   = "dddb-20150724"
#LHCbApp().CondDBtag = "sim-20161124-2-vc-md100"

#Upgrade Tags
#LHCbApp().DDDBtag   = "dddb-20170228"
#LHCbApp().CondDBtag = "sim-20170301-vc-mu100"

#Tags from Gauss step
LHCbApp().DDDBtag = "dddb-20160304"
LHCbApp().CondDBtag = "sim-20150716-vc-md100"
LHCbApp().EvtMax = 5

B.Outputs = ['DIGI']
OutputStream("DigiWriter").Output = "DATAFILE='PFN:minbias_upgrade_hiLumi.xdigi'"
B.DigiType = "Extended"
B.OutputLevel = INFO
CondDB().Upgrade = True

from Configurables import BooleInit
BooleInit().SetOdinRndTrigger = True

B.InputDataType = "SIM"
B.Histograms = "Default"
HistogramPersistencySvc().OutputFile = "booleHistos.root"
#B.DataType = "2016"
B.DataType = "Upgrade"

#Options from Upgrade production
importOptions("$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py")
importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")

#mypdb.set_trace()
#L0Conf().TCK = '0x1609'
B.MainSequence = ["ProcessPhase/Init",
                  "ProcessPhase/Digi",
                  "ProcessPhase/Link",
                  "ProcessPhase/Moni",
                  "ProcessPhase/Filter"]

B.DetectorInit = {"DATA" : ['Data'], "MUON":['Muon']}
B.DetectorDigi = ['UT']
B.DetectorLink = ['UT']
B.DetectorMoni = ['UT']

from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['Gauss-30000000_hiLumi_production_nospill.sim'],clear=True)

#IOHelper('ROOT').inputFiles(['Gauss-11876061_2016.xgen'],clear=True)
