#################
# CONFIGURATION #
#################
nEvts = 5
EventType = '30000000'
#EventType = '11876060'
#################
#################
from Gauss.Configuration import *

#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber        = 1082

#--Number of events
LHCbApp().EvtMax = nEvts

#solely generator phase
#Gauss().Phases = ["Generator","GenToMCTree"]

idFile = './Gauss-'+EventType
HistogramPersistencySvc().OutputFile = idFile+'-histos_UTOnly_latestTags.root'
OutputStream("GaussTape").Output = "DATAFILE='PFN:%s_hiLumi_UTOnly_latestTags.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"%idFile

#-- import options

#importOptions('$APPCONFIGOPTS/Gauss/xgen.py')
importOptions('$APPCONFIGOPTS/Gauss/xsim.py')

#############################
#--Pick beam conditions as set in AppConfig

#Nominal 2016
#importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-nu1.6.py")

#Upgrade with nominal Lumi
#importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu7.6.py")

#Upgrade with 10xLumi
importOptions("./Beam7000GeV-md100-nu7.6.py")
importOptions('./Gen/DecFiles/options/'+EventType+'.py')
##############################
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")
#importOptions("$APPCONFIGOPTS/Gauss/DataType-2016.py")
#from Configurables import Gauss
#Gauss().DataType  = "Upgrade"

importOptions("$APPCONFIGOPTS/Conditions/Upgrade.py")
#importOptions("$APPCONFIGOPTS/Gauss/RICHRandomHits.py")

importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")
importOptions("$APPCONFIGOPTS/Gauss/Gauss-Upgrade-VP_Compact_MicroChannel-UT-FT.py");
importOptions("$APPCONFIGOPTS/Conditions/Upgrade-FT_MonoLayer.py");

#importOptions("$GAUSSOPTS/GenStandAlone.py")
#--Set database tags using those for 2016
from Configurables import LHCbApp

#2016 data type 
#LHCbApp().DDDBtag   = "dddb-20150724"
#LHCbApp().CondDBtag = "sim-20161124-2-vc-md100"

#Upgrade data type
LHCbApp().DDDBtag   = "dddb-20130806"
LHCbApp().CondDBtag = "sim-20130722-vc-md100"

#Latest Upgrade Tags
#LHCbApp().DDDBtag = "dddb-20170301"
#LHCbApp().CondDBtag = "sim-20170301-vc-md100"

#--Pick up new particle table until it is in a global tag
#from Configurables import CondDB
#CondDB().LocalTags = { "DDDB":["particles-20150720"] }



