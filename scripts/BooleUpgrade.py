############################################################################                                                                                        
# File for running Boole with default settings (full Digi output)                                                                                                   
#                                                                                                                                                                   
# Syntax is:                                                                                                                                                        
# gaudirun.py Boole/Default.py Conditions/<someTag>.py <someDataFiles>.py                                                                                           
############################################################################                                                                                        
from Configurables import Boole, CondDB
from Boole.Configuration import *
                                                                            
Boole().UseSpillover = True
Boole().SpilloverPaths = ["PrevPrev", "Prev", "Next", "NextNext"]
Boole().DataType  = "Upgrade"
Boole().Outputs = [ "DIGI" ]
Boole().DigiType = "Extended"
#Boole().DetectorDigi = ['VP','UT','FT']
#Boole().DetectorLink = ['VP','UT','FT']
#Boole().DetectorMoni = ['VP','UT','FT']
Boole().DetectorDigi = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt']
Boole().DetectorLink = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Tr']
Boole().DetectorMoni = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Tr', 'MC']

Boole().InputDataType = "SIM"

CondDB().Upgrade = True

#tags from Laurent
#Boole().DDDBtag = "dddb-20170726"
#Boole().CondDBtag = "sim-20170301-vc-mu100"

#Latest (20.01.2020) Upgrade (1) Tags
LHCbApp().DDDBtag = "dddb-20190223"
LHCbApp().CondDBtag = "sim-20180530-vc-mu100"

#tags and option from Upgrade MC central productions - FT geom 6.3 too new and too many detectors
#Boole().DDDBtag = "dddb-20171126"
#Boole().CondDBtag = "sim-20171127-vc-md100"
#importOptions("$APPCONFIGOPTS/Boole/Default.py")
#importOptions("$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20150522.py")
#importOptions("$APPCONFIGOPTS/Boole/EnableSpillover.py")
#importOptions("$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py") 

from Configurables import OutputStream
#EventSelector().Input = ["DATAFILE='PFN:/eos/lhcb/user/l/lgrillo/Gauss-30000000_hiLumi_production_nospill.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
#EventSelector().Input = ["DATAFILE='PFN:/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/minibias/minibias_MagUp_1_Gauss.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
#EventSelector().Input = ["DATAFILE='PFN:/eos/lhcb/lbdt3/user/ldufour/mighty-tracker/2e34_samples_new/5099_MagUp_bs2phiphi_1p5e34.sim' TYP='POOL_ROOTTREE' OPT='READ'", "DATAFILE='PFN:/eos/lhcb/lbdt3/user/ldufour/mighty-tracker/2e34_samples_new/5098_MagUp_bs2phiphi_1p5e34.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
#EventSelector().Input = ["DATAFILE='PFN:/eos/lhcb/lbdt3/user/ldufour/mighty-tracker/2e34_samples_new/5522_MagUp_bs2phiphi_1p5e34.sim' TYP='POOL_ROOTTREE' OPT='READ'", "DATAFILE='PFN:/eos/lhcb/lbdt3/user/ldufour/mighty-tracker/2e34_samples_new/5738_MagUp_bs2phiphi_1p5e34.sim' TYP='POOL_ROOTTREE' OPT='READ'", "DATAFILE='PFN:/eos/lhcb/lbdt3/user/ldufour/mighty-tracker/2e34_samples_new/5604_MagUp_bs2phiphi_1p5e34.sim' TYP='POOL_ROOTTREE' OPT='READ'", "DATAFILE='PFN:/eos/lhcb/lbdt3/user/ldufour/mighty-tracker/2e34_samples_new/5702_MagUp_bs2phiphi_1p5e34.sim' TYP='POOL_ROOTTREE' OPT='READ'", "DATAFILE='PFN:/eos/lhcb/lbdt3/user/ldufour/mighty-tracker/2e34_samples_new/5537_MagUp_bs2phiphi_1p5e34.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
#OutputStream("DigiWriter").Output = '5522_5738_5604_5702_5537_MagUp_bs2phiphi_1p5e34.xdigi'
#EventSelector().Input = ["DATAFILE='PFN:/afs/cern.ch/work/l/lgrillo/MightyIT/MightyMatchingTiming/MightyIT/DigiGeneration/Gauss-13104012_hiLumi_production_nospill.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
#OutputStream("DigiWriter").Output = 'Boole_MagUp_bs2phiphi.xdigi'
#EventSelector().Input = ["DATAFILE='PFN:/afs/cern.ch/work/l/lgrillo/MightyIT/MightyMatchingTiming/MightyIT/DigiGeneration/Gauss-30000000_hiLumi_production_nospill.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
#OutputStream("DigiWriter").Output = 'Boole_MagUp_30000000.xdigi'
EventSelector().Input = ["DATAFILE='PFN:/afs/cern.ch/user/q/qzou/eos/qzou/utSim/20evt/test/hiLumi_production_nospill_ev10.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
OutputStream("DigiWriter").Output = 'PFN:./Boole_MagUp_30000000_3evt.xdigi'
############################################################################                                                                                        

