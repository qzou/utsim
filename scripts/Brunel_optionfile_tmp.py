###############################################################################
# File for running alignment with D0 reconstructed from HLT sel reports
###############################################################################
# Syntax is:
#   gaudirun.py Brunel-testMC-simple.py <someDataFiles>.py
###############################################################################

import os, sys
#os.environ['NO_GIT_CONDDB'] = '1'

from Configurables import Brunel, LHCbApp, CondDB, NTupleSvc, DigiConf
from Gaudi.Configuration import *

# Just instantiate the configurable...
theApp = Brunel()
theApp.DataType   = "Upgrade"
theApp.InputType  = "DIGI"#xdigi
DigiConf().DigiType = "Extended"
theApp.WithMC = True
theApp.Simulation = True
#theApp.Detectors  = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Tr' ]
#theApp.RecoSequence = ["Decoding","TrFast","TrBest","RICH","CALO","MUON","PROTO","SUMMARY"]
theApp.Detectors  = ['VP', 'UT', 'FT', 'Tr' ]
theApp.RecoSequence = ["Decoding","TrFast", "TrBest", "PROTO"]
theApp.Monitors = []

theApp.PrintFreq = 10
#theApp.EvtMax = 50
theApp.EvtMax = -1
#theApp.MCLinksSequence = [ "Tr"] #LDST?
theApp.OutputType = 'ROOT'
#theApp.SplitRawEventInput = 4.1
#theApp.SplitRawEventOutput = 4.1
#theApp.DatasetName = 'MC-reso2017-SigMC-NoGP'
theApp.DatasetName = 'MChists'

#theApp.DDDBtag   = "dddb-20150724"
#theApp.CondDBtag   = "sim-20160606-vc-md100" 

#theApp.DDDBtag   = "dddb-20150724"
#theApp.CondDBtag   = "sim-20161124-2-vc-md100"

# Tags for Brunel v52r6p1 - FT Geom 61 needed
#theApp.DDDBtag   = "dddb-20170301"
#theApp.CondDBtag   = "sim-20170301-vc-mu100"

# Tags for Brunel v49r0
#theApp.DDDBtag   = "dddb-20150729"
#theApp.CondDBtag   = "sim-20150716-vc-mu100"

# Tags for Brunel v51r0 from Renato
CondDB().Upgrade = True
#For Yunlong
#theApp.DDDBtag    = "dddb-20150424" # Upgrade database tags
#theApp.CondDBtag  = "sim-20140204-vc-md100"
#Laurent
#Boole().DDDBtag   = "dddb-20171010"
#Boole().CondDBtag = "sim-20180530-vc-md100"
theApp.DDDBtag = "dddb-20171010"#"dddb-20170726"
theApp.CondDBtag = "sim-20170301-vc-md100"


#from Configurables import MessageSvc
#MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#from Configurables import DstConf
#DstConf(EnableUnpack=["Reconstruction", "Stripping"])

#from Configurables import DecodeRawEvent
#DecodeRawEvent().setProp("OverrideInputs",4.1)
from Configurables import CondDBAccessSvc, CondDB, UpdateManagerSvc, L0Conf, OutputStream, NTupleSvc


from Gaudi.Configuration import appendPostConfigAction
#CondDB().UseOracle = False
#CondDB().IgnoreHeartBeat= True
#CondDB().EnableRunStampCheck = False
#CondDB().LocalTags["SIMCOND"] = ["ot-20160722"]

L0Conf().EnsureKnownTCK=False

OutputStream("DstWriter").ItemList([  
        ''
         ], clear=True)


#Digi files in input
from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
    #Differnt beam conditions
	'/afs/cern.ch/user/q/qzou/eos/qzou/utSim/20evt/test/Boole_MagUp_30000000_3evt.xdigi'
], clear=True)

