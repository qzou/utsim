#################
# CONFIGURATION #
#########~gauss_highLumi.py~########
nEvts =20
EventType = '30000000'
#EventType = '11876060'
#EventType = '13104012'#Bs_phiphi=CDFAmp,DecProdCut.dec

#################
#################
from Gauss.Configuration import *

#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 10
GaussGen.RunNumber        = 1093

#--Number of events
LHCbApp().EvtMax = nEvts

#solely generator phase
#Gauss().Phases = ["Generator","GenToMCTree"]

idFile = './Gauss-'+EventType
HistogramPersistencySvc().OutputFile = idFile+'-histos_production_nospill_ev10.root'
OutputStream("GaussTape").Output = "DATAFILE='PFN:/afs/cern.ch/user/q/qzou/eos/qzou/utSim/20evt/hiLumi_production_nospill_ev10.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"

#-- import options

#importOptions('$APPCONFIGOPTS/Gauss/xgen.py')
importOptions('$APPCONFIGOPTS/Gauss/xsim.py')

#############################
#--Pick beam conditions as set in AppConfig

#Nominal 2016
#importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-nu1.6.py")

#Upgrade with nominal Lumi
#importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu7.6.py")

#Upgrade with 10xLumi
#importOptions("./Beam7000GeV-md100-nu7.6.py")
importOptions("./beam_upgradeII.py")

#importOptions("$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py")
#importOptions('./Gen/DecFiles/options/'+EventType+'.py')
importOptions('$DECFILESROOT/options/'+EventType+'.py')
##############################
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
#importOptions("$APPCONFIGOPTS/Gauss/Gauss-Upgrade-Baseline-20150522.py")
importOptions("./Gauss-Upgrade-Baseline-20150522-TrackerOnly.py")
#importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")
#importOptions("$APPCONFIGOPTS/Gauss/DataType-2016.py")
#from Configurables import Gauss
#Gauss().DataType  = "Upgrade"

#importOptions("$APPCONFIGOPTS/Conditions/Upgrade.py")
#importOptions("$APPCONFIGOPTS/Gauss/RICHRandomHits.py")

#Tracker Only? - I haven't tried yet
importOptions("$APPCONFIGOPTS/Gauss/RICHRandomHits.py")
#importOptions("$APPCONFIGOPTS/Gauss/no_calo_and_muon.py")

importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")
#importOptions("$APPCONFIGOPTS/Gauss/Gauss-Upgrade-VP_Compact_MicroChannel-UT-FT.py");
#importOptions("$APPCONFIGOPTS/Conditions/Upgrade-FT_MonoLayer.py");

#importOptions("$GAUSSOPTS/GenStandAlone.py")
#--Set database tags using those for 2016
from Configurables import LHCbApp

#2016 data type 
#LHCbApp().DDDBtag   = "dddb-20150724"
#LHCbApp().CondDBtag = "sim-20161124-2-vc-md100"

#Upgrade data type
#LHCbApp().DDDBtag   = "dddb-20130806"
#LHCbApp().CondDBtag = "sim-20130722-vc-md100"

#Latest Upgrade Tags
#LHCbApp().DDDBtag = "dddb-20170301"
#LHCbApp().CondDBtag = "sim-20170301-vc-md100"

#Upgrade tags from summer 2016 trigger studies
#LHCbApp().DDDBtag = "dddb-20160304"
#LHCbApp().CondDBtag = "sim-20150716-vc-md100"

# Tags for Brunel v51r0 from Renato
#CondDB().Upgrade = True
#LHCbApp().DDDBtag    = "dddb-20150424" # Upgrade database tags
#LHCbApp().CondDBtag  = "sim-20140204-vc-md100"

# Tags from Laurent
#LHCbApp().DDDBtag = "dddb-20170726"
#LHCbApp().CondDBtag = "sim-20170301-vc-mu100"

#Latest (20.01.2020) Upgrade (1) Tags
LHCbApp().DDDBtag = "dddb-20190223"
LHCbApp().CondDBtag = "sim-20180530-vc-mu100"

#--Pick up new particle table until it is in a global tag
#from Configurables import CondDB
#CondDB().LocalTags = { "DDDB":["particles-20150720"] }

