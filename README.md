# UT detector simulation

## Baseline
**software**

* Gauss
* Boole
* Brunel

Basically, we use Gauss to generate MC events, use Boole/Brunel to extract info from a .sim or .digi file. 
The way we use to extract info can be found in following content.
To fit the upgrade situation, you have to config your Gauss, like beam condition etc.  

## Add an algorithm to Boole
The principal for putting an algorithm into Boole is nothing but making a local compiled version.

```
lb-dev Boole/v40r1
git lb-use Rec 
git lb-checkout Rec/2018-patches Tr/PatChecker
```
By using lb-checkout, you can get a patch package named 'Tr', where you can put your own algorithm. If your want to write a standalone
package to place your algorithm, it's fine. Usually, you can save your time and use this existed package.

**Notice**

* It's better to delete all existed algorithms under Tr/PatChecker/src, which are out of time and can hardly pass the compiling process.
* Make sure you have a related DDDB tag & sim tag for a specific version, they are highly related. A recent version usually works.
* Use the same tag for Gauss and following software, the tag is key for Gauss and Boole to do the generation and digitalization. But it's not so important when you just use the software to extract MCHits and so on. 

## files in this repository

The files are usually classified by its function, eg. you can find algorithms under the folder 'algorithms'
Since it's just a developing repository, it's fine to make any changes if you want.

## Things to check 

Very first try to extract info from *.digi* file,this is based on Brunel v60r0 by now.
The Cluster & MCHits/MCParticle links are acquired by LinkFrom class.

In the links,we use 'cluster->channel()', it seems that this links to the liteCluster channel which just has one channel for a Cluster. But in real situation, one cluster have many related strips reponses. **CHECK how does cluster link to MC(use strip channel id? the indexKey for cluster?)**


## Simulation condition
You can find a log file under the folder 'log', whcih records a recent project of extracting MCHits from .sim file.
*To be updated...*

## ISSUE
- In Gauss, they changed the DecFiles to v31r5，this may cause some problems when running.
You can refer to this link:https://gitlab.cern.ch/lhcb/Gauss/-/merge_requests/712
