// $Id: PatCheckerNTuple.h,v 1.1.1.1 2007-10-09 18:41:19 smenzeme Exp $                                                                                                                                                                                                                          
#ifndef MCVerticesTuple_H
#define MCVerticesTuple_H 1

// Include files

// from Gaudi                                                                                                                                                                                                                                                                    
#include "GaudiAlg/GaudiTupleAlg.h"

class MCVerticesTuple: public GaudiTupleAlg
{
 public:
  /// Standard constructor
  MCVerticesTuple(const std::string& name, ISvcLocator* pSvcLocator);

  virtual ~MCVerticesTuple(); ///< Destructor
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization
 
  int fromMCVertex(const LHCb::MCParticle* mcParticle, const LHCb::MCVertex* mcVertex);

 protected:

 private:
  std::vector<std::string> m_dets;
  std::string m_outputData;  ///< location of Particles to associate                                                                                                                                                                                                                           
  std::string m_inputData;
  std::string m_tupleName;
};

#endif // MCVerticesTuple_H  
