// Include files

#include "Event/Track.h"
#include "Event/MCParticle.h"
#include "Event/MCHit.h"
#include "Event/MCVertex.h"
#include "Event/ODIN.h"

// from Event/LinkerEvent
#include "Linker/LinkedTo.h"
#include "Linker/LinkerWithKey.h"
#include "Linker/LinkedFrom.h"

// local
#include "MCVerticesTuple.h"

#include "TMath.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT (MCVerticesTuple)

/**
 * Implementation for MCVerticesTuple.
 *
 * Just dumps t/x/y/z for MCVertices
 */

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCVerticesTuple::MCVerticesTuple(const std::string& name, ISvcLocator* pSvcLocator) :
GaudiTupleAlg ( name, pSvcLocator )
{
  declareProperty ( "NtupleName", m_tupleName = "Vertices" );
  declareProperty ( "OutputData", m_outputData = "MC/Particles2MCTrackerHits" );
  declareProperty ( "Detectors", m_dets = {"VP", "UT", "FT"});
}

//=============================================================================
// Destructor
//=============================================================================
MCVerticesTuple::~MCVerticesTuple()
{
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MCVerticesTuple::initialize()
{
  StatusCode sc = GaudiTupleAlg::initialize (); // must be executed first
  if (sc.isFailure ())
    return sc;  // error printed already by GaudiAlgorithm

  debug () << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MCVerticesTuple::execute()
{
  const LHCb::ODIN* odin = getIfExists<LHCb::ODIN>(LHCb::ODINLocation::Default);

  if (!odin)
    return StatusCode::SUCCESS;

  std::vector<const LHCb::MCParticle*> mcParts;

  LHCb::MCVertices* mcVertices = get<LHCb::MCVertices>(LHCb::MCVertexLocation::Default);
  std::vector<const LHCb::MCVertex*> vtcsMCPV;
  int nrMCPVs = 0;
  for(LHCb::MCVertices::const_iterator itMCV = mcVertices->begin();
      mcVertices->end() != itMCV; itMCV++) {
    nrMCPVs++;
    const LHCb::MCParticle* motherPart = (*itMCV)->mother();

    debug() << " motherPart =  " << motherPart  <<endmsg;
    if(0 == motherPart) {
      if((*itMCV)->type() == LHCb::MCVertex::ppCollision) {
        const LHCb::MCVertex* mcVtx = *itMCV;
        int isfromMCPV = 0;
        for(std::vector<const LHCb::MCParticle*>::iterator
	      itP = mcParts.begin(); itP != mcParts.end(); itP++) {
          const LHCb::MCParticle* mcPart = *itP;
          isfromMCPV = isfromMCPV + fromMCVertex(mcPart,mcVtx);
        }
        if(isfromMCPV > 4) {
          vtcsMCPV.push_back(*itMCV);
	}
	debug() << " Vertex time =  " << mcVtx->position4vector().t()  <<endmsg;
	
	// Fill the ntuple                                                                                                                                                                                                                                                                 
	Tuple tPoint = nTuple ( m_tupleName );
	tPoint->column ( "pvtime", mcVtx->position4vector().t() );
	tPoint->column ( "pvx", mcVtx->position4vector().x() );
	tPoint->column ( "pvy", mcVtx->position4vector().y() );
	tPoint->column ( "pvz", mcVtx->position4vector().z() );
	
	tPoint->column ( "pvz", mcVtx->position4vector().z() );
	
	tPoint->column ( "eventNumber", odin->eventNumber() );
	tPoint->column ( "isfromMCPV", isfromMCPV);

	tPoint->write ();
       
      }
    }
  }


  return StatusCode::SUCCESS;
}

//=============================================================================                                                                                                                                                                                                                 // MC particle from MC visible PV                                                                                                                                                                                                                                                               //=============================================================================                                                                                                                                                                                                                  
int MCVerticesTuple::fromMCVertex(const LHCb::MCParticle* mcParticle,
                                         const LHCb::MCVertex* mcVertex)
{
  int isDaugh = 0;
  const LHCb::MCVertex* mcVtx = 0;
  const LHCb::MCParticle* motherMC = mcParticle->mother();
  while(motherMC) {
    mcVtx = motherMC->originVertex();
    motherMC = motherMC->mother();
  }
  if(mcVertex == mcVtx) isDaugh = 1;
  return isDaugh;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode MCVerticesTuple::finalize()
{
  return GaudiTupleAlg::finalize ();  // must be called after all other actions
}
