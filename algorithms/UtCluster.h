// $Id: PatCheckerNTuple.h,v 1.1.1.1 2007-10-09 18:41:19 smenzeme Exp $
#ifndef MCPARTICLENTUPLE_H
#define MCPARTICLENTUPLE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"


/** @class UtCluster UtClsuter.h
 *
 *   Algorithm to produce an NTuple to check Pat tracks.
 *  @author Olivier Callot
 *  @date   2006-05-11
 *  @update for A-Team framework 2007-08-20
 *  @update Lucia Grillo (study of MCHits and tracks information)
 */


  class UtCluster : public GaudiTupleAlg {
  public:
    /// Standard constructor
    UtCluster( const std::string& name, ISvcLocator* pSvcLocator );

    virtual ~UtCluster( ); ///< Destructor

    StatusCode initialize() override;    ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;    ///< Algorithm finalization

  protected:

  private:
    std::string m_tupleName;
    std::string m_container;
    std::vector<std::string> m_dets;
    std::string m_outputData;  ///< location of Particles to associate
    std::string m_inputData;
    Gaudi::Property<std::string> m_mcUTHits                    {this, "MCHitsLocation", "/Event/MC/UT/Hits"};
    Gaudi::Property<std::string> m_UTcluster2ParticleLinkerLocation { this, "UTcluster2ParticleLinkerLocation",LHCb::STClusterLocation::UTClusters};
    Gaudi::Property<std::string> m_UTcluster2MCHitsLinkerLocatin     { this, "UTcluster2MCHitsLinkerLocatin", LHCb::STClusterLocation::UTClusters + "2MCHits"};
    //Gaudi::Property<std::string> m_hitManagerName                     { this, "HitManagerName", "PrFTHitManager"};
    //void mcstudy(){};
    //void test(){};

  };

#endif // MCPARTICLENTUPLE_H
