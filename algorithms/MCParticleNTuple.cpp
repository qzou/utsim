// Include files

#include "Event/Track.h"
#include "Event/MCParticle.h"
#include "Event/MCHit.h"
#include "Event/ODIN.h"

// from Event/LinkerEvent
#include "Linker/LinkedTo.h"
#include "Linker/LinkerWithKey.h"
#include "Linker/LinkedFrom.h"

// local
#include "MCParticleNTuple.h"

#include "TMath.h"


//-----------------------------------------------------------------------------
// Implementation file for class : MCParticleNTuple
//
// 2018-06-01 : Lucia Grillo (starting from Olivier Callot PatCheckerNTuple)
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCParticleNTuple )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCParticleNTuple::MCParticleNTuple( const std::string& name,
                                    ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator )
{
  declareProperty( "NtupleName"           , m_tupleName = "Tracks"    );
  declareProperty( "OutputData",  m_outputData =
                   "MC/Particles2MCTrackerHits" );
  declareProperty("Detectors", m_dets = {"VP", "UT", "FT"});
  //declareProperty("Detectors", m_dets = {"FT"});
  //declareProperty( "OutputData",  m_outputData = LHCb::MCParticleLocation::Default + "2MCHits" );
  //declareProperty( "MCHitPath",   m_inputData );
  //declareProperty( "OutputData",  m_outputData );
  //  declareProperty( "InputTracks"          , m_container = LHCb::TrackLocation::Forward );
}
//=============================================================================
// Destructor
//=============================================================================
MCParticleNTuple::~MCParticleNTuple() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MCParticleNTuple::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MCParticleNTuple::execute() {

  debug() << "==> Execute" << endmsg;

  const LHCb::MCParticle::Container* mcparticles = get<LHCb::MCParticle::Container>( LHCb::MCParticleLocation::Default );

  // make the output table
  LinkerWithKey<LHCb::MCParticle,LHCb::MCHit> myLink( eventSvc(), msgSvc(), m_outputData );


  //// get MCHits FT
  LHCb::MCHits* mcHits = get<LHCb::MCHits>(  "/Event/MC/FT/Hits" );

  //// loop over MCHits
  for(LHCb::MCHits::const_iterator itHit = mcHits->begin();itHit != mcHits->end(); ++itHit) {
    LHCb::MCHit* mcHit = *itHit ;
    if (!mcHit) {
      return Error( "Failed retrieving MCHit" );
    }
    myLink.link( mcHit, mcHit->mcParticle() );
  }

  //// get MCHits VP
  LHCb::MCHits* mcVPHits = get<LHCb::MCHits>(  "/Event/MC/VP/Hits" );

  //// loop over MCHits
  for(LHCb::MCHits::const_iterator itVPHit = mcVPHits->begin();itVPHit != mcVPHits->end(); ++itVPHit) {
    LHCb::MCHit* mcVPHit = *itVPHit ;
    if (!mcVPHit) {
      return Error( "Failed retrieving MCVPHit" );
    }
    myLink.link( mcVPHit, mcVPHit->mcParticle() );
  }

  //// get MCHits UT
  LHCb::MCHits* mcUTHits = get<LHCb::MCHits>(  "/Event/MC/UT/Hits" );

  //// loop over MCHits
  for(LHCb::MCHits::const_iterator itUTHit = mcUTHits->begin();itUTHit != mcUTHits->end(); ++itUTHit) {
    LHCb::MCHit* mcUTHit = *itUTHit ;
    if (!mcUTHit) {
      return Error( "Failed retrieving MCUTHit" );
    }
    myLink.link( mcUTHit, mcUTHit->mcParticle() );
  }


  warning() << "Linker table stored at " << m_outputData << endmsg;
  // save some typing
  LinkedFrom<LHCb::MCHit,LHCb::MCParticle> HitLinks( evtSvc(), msgSvc(), m_outputData );

  for ( LHCb::MCParticle::Container::const_iterator itT = mcparticles->begin(); mcparticles->end() != itT ; ++itT ) {
    LHCb::MCParticle* part = *itT;

    LHCb::MCHit* aHit = HitLinks.first( part );
    std::vector<LHCb::MCHit*> Hits_of_track;

    std::vector<float>Hist_of_track_Zpos_en;
    std::vector<float>Hist_of_track_Xpos_en;
    std::vector<float>Hist_of_track_Ypos_en;


    std::vector<float>Hist_of_track_Zpos_ex;
    std::vector<float>Hist_of_track_Xpos_ex;
    std::vector<float>Hist_of_track_Ypos_ex;

    std::vector<float>Hist_of_track_Tpos;
    //Gaudi::XYZPoint mcHitPosition;

    std::vector<float>PathLengths;

    while ( aHit ) {
      Hits_of_track.push_back( aHit );
      Hist_of_track_Zpos_en.push_back( aHit->entry().z() );
      Hist_of_track_Xpos_en.push_back( aHit->entry().x() );
      Hist_of_track_Ypos_en.push_back( aHit->entry().y() );

      Hist_of_track_Zpos_ex.push_back( aHit->exit().z() );
      Hist_of_track_Xpos_ex.push_back( aHit->exit().x() );
      Hist_of_track_Ypos_ex.push_back( aHit->exit().y() );
      
      Hist_of_track_Tpos.push_back( aHit->time() );
      PathLengths.push_back( aHit->pathLength() );
      aHit = HitLinks.next();
    }

    float TotalPathLength = 0.0;
    int k=0;
    for( int k = 0; k<(PathLengths.size()); k++){
      TotalPathLength+=PathLengths.at(k);
    }
    float PathLengthExample = 0.0;
    if(PathLengths.size()>0){
      float PathLengthExample = PathLengths.at(0);
    }
    int NHitEntries = Hist_of_track_Zpos_en.size();
    /*float array_Hist_of_track_Zpos[30] = {0.0};
    float array_Hist_of_track_Xpos[30] = {0.0};
    float array_Hist_of_track_Ypos[30] = {0.0};
    float array_Hist_of_track_Tpos[30] = {0.0};

    float array_Velo_Hist_of_track_Zpos[60] = {0.0};
    float array_Velo_Hist_of_track_Xpos[60] = {0.0};
    float array_Velo_Hist_of_track_Ypos[60] = {0.0};
    float array_Velo_Hist_of_track_Tpos[60] = {0.0};

    float array_UT_Hist_of_track_Zpos_en[60] = {0.0};
    float array_UT_Hist_of_track_Xpos_en[60] = {0.0};
    float array_UT_Hist_of_track_Ypos_en[60] = {0.0};

    float array_UT_Hist_of_track_Zpos_ex[60] = {0.0};
    float array_UT_Hist_of_track_Xpos_ex[60] = {0.0};
    float array_UT_Hist_of_track_Ypos_ex[60] = {0.0};

    float array_UT_Hist_of_track_Tpos[60] = {0.0};


    float array_VeloUT_Hist_of_track_Zpos[60] = {0.0};
    float array_VeloUT_Hist_of_track_Xpos[60] = {0.0};
    float array_VeloUT_Hist_of_track_Ypos[60] = {0.0};
    float array_VeloUT_Hist_of_track_Tpos[60] = {0.0};*/
    //global info
    float array_Hist_of_track_Zpos_en[150] = {0.0};
    float array_Hist_of_track_Xpos_en[150] = {0.0};
    float array_Hist_of_track_Ypos_en[150] = {0.0};

    float array_Hist_of_track_Zpos_ex[150] = {0.0};
    float array_Hist_of_track_Xpos_ex[150] = {0.0};
    float array_Hist_of_track_Ypos_ex[150] = {0.0};

    float array_Hist_of_track_Tpos[150] = {0.0};

    //specific UT
    float array_UT_Hist_of_track_Zpos_en[60] = {0.0};
    float array_UT_Hist_of_track_Xpos_en[60] = {0.0};
    float array_UT_Hist_of_track_Ypos_en[60] = {0.0};

    float array_UT_Hist_of_track_Zpos_ex[60] = {0.0};
    float array_UT_Hist_of_track_Xpos_ex[60] = {0.0};
    float array_UT_Hist_of_track_Ypos_ex[60] = {0.0};

    float array_UT_Hist_of_track_Tpos[60] = {0.0};

    k=0;
    int flag=0;
if(NHitEntries>0){
      for( int i = 0; i<(NHitEntries); i++){

      	  array_Hist_of_track_Zpos_en[k] = Hist_of_track_Zpos_en.at(i);
      	  array_Hist_of_track_Xpos_en[k] = Hist_of_track_Xpos_en.at(i);
      	  array_Hist_of_track_Ypos_en[k] = Hist_of_track_Ypos_en.at(i);

          array_Hist_of_track_Zpos_ex[k] = Hist_of_track_Zpos_ex.at(i);
      	  array_Hist_of_track_Xpos_ex[k] = Hist_of_track_Xpos_ex.at(i);
      	  array_Hist_of_track_Ypos_ex[k] = Hist_of_track_Ypos_ex.at(i);
      	  array_Hist_of_track_Tpos[k] = Hist_of_track_Tpos.at(i);
      	  k = k +1;
          
	      //debug()<< " NHitEntries = " << NHitEntries << " i = " << i << " Hist_of_track_Zpos_en.at(i) " << Hist_of_track_Zpos_en.at(i) << " k = " << k << " array_Hist_of_track_Zpos[k] "<< array_Hist_of_track_Zpos[k] <<endmsg;
      }
}

k=0;
if(NHitEntries>0){
      for( int j= 0; j<(NHitEntries); j++){

      	if(Hist_of_track_Zpos_ex.at(j)<3000&&Hist_of_track_Zpos_en.at(j)>2000){

      	  array_UT_Hist_of_track_Zpos_en[k] = Hist_of_track_Zpos_en.at(j);
      	  array_UT_Hist_of_track_Xpos_en[k] = Hist_of_track_Xpos_en.at(j);
      	  array_UT_Hist_of_track_Ypos_en[k] = Hist_of_track_Ypos_en.at(j);

          array_UT_Hist_of_track_Zpos_ex[k] = Hist_of_track_Zpos_ex.at(j);
      	  array_UT_Hist_of_track_Xpos_ex[k] = Hist_of_track_Xpos_ex.at(j);
      	  array_UT_Hist_of_track_Ypos_ex[k] = Hist_of_track_Ypos_ex.at(j);
      	  array_UT_Hist_of_track_Tpos[k] = Hist_of_track_Tpos.at(j);
      	  k = k+1;
          flag+=1;
      	}
      	else
      	  continue;


	      //debug()<< " NHitEntries = " << NHitEntries << " j= " << j<< " Hist_of_track_Zpos.at(j) " << Hist_of_track_Zpos.at(j) << "k = " << k <<  " array_UT_Hist_of_track_Zpos[k] "<< array_UT_Hist_of_track_Zpos[k]<<endmsg;
      }
}
    /*k = 0;

    if(Hist_of_track_Zpos.size()>0){
      for( int i = 0; i<(NHitEntries); i++){

      	if(Hist_of_track_Zpos.at(i)>7000){
      	  array_Hist_of_track_Zpos[k] = Hist_of_track_Zpos.at(i);
      	  array_Hist_of_track_Xpos[k] = Hist_of_track_Xpos.at(i);
      	  array_Hist_of_track_Ypos[k] = Hist_of_track_Ypos.at(i);
      	  array_Hist_of_track_Tpos[k] = Hist_of_track_Tpos.at(i);
      	  k = k +1;
      	}
      	else
      	  continue;

	      debug()<< " NHitEntries = " << NHitEntries << " i = " << i << " Hist_of_track_Zpos.at(i) " << Hist_of_track_Zpos.at(i) << " k = " << k << " array_Hist_of_track_Zpos[k] "<< array_Hist_of_track_Zpos[k] <<endmsg;


      }

      k = 0;

      for( int j= 0; j<(NHitEntries); j++){

      	if(Hist_of_track_Zpos.at(j)<800){
      	  array_Velo_Hist_of_track_Zpos[k] = Hist_of_track_Zpos.at(j);
      	  array_Velo_Hist_of_track_Xpos[k] = Hist_of_track_Xpos.at(j);
      	  array_Velo_Hist_of_track_Ypos[k] = Hist_of_track_Ypos.at(j);
      	  array_Velo_Hist_of_track_Tpos[k] = Hist_of_track_Tpos.at(j);
      	  k = k+1;
      	}
      	else
      	  continue;


	      debug()<< " NHitEntries = " << NHitEntries << " j= " << j<< " Hist_of_track_Zpos.at(j) " << Hist_of_track_Zpos.at(j) << "k = " << k <<  " array_Velo_Hist_of_track_Zpos[k] "<< array_Velo_Hist_of_track_Zpos[k]<<endmsg;


      }

      

      k=0;

      for( int j= 0; j<(NHitEntries); j++){

        if(Hist_of_track_Zpos.at(j)>7000){
          continue;
        }
        else{
          array_VeloUT_Hist_of_track_Zpos[k] = Hist_of_track_Zpos.at(j);
          array_VeloUT_Hist_of_track_Xpos[k] = Hist_of_track_Xpos.at(j);
          array_VeloUT_Hist_of_track_Ypos[k] = Hist_of_track_Ypos.at(j);
          array_VeloUT_Hist_of_track_Tpos[k] = Hist_of_track_Tpos.at(j);
          k = k+1;
        }

        debug()<< " NHitEntries = " << NHitEntries << " j= " << j<< " Hist_of_track_Zpos.at(j) " << Hist_of_track_Zpos.at(j) << "k = " << k <<  " array_VeloUT_Hist_of_track_Zpos[k] "<< array_VeloUT_Hist_of_track_Zpos[k]<<endmsg;
      }

    }
    else
      debug() << " NHitEntries (expect 0) = " << NHitEntries <<endmsg;*/

    const LHCb::ODIN* odin = getIfExists<LHCb::ODIN>(LHCb::ODINLocation::Default);

    //debug() << " array_Hist_of_track_Zpos = " << array_Hist_of_track_Zpos <<endmsg;

    Gaudi::XYZVector mcSlopes  = part->momentum().Vect();
    mcSlopes /= mcSlopes.z();
    double mcMomentum = part->momentum().P();
    double Mass=part->momentum().M(); 
    double mcMomentum_z = part->momentum().Pz();
    int ParticleID = part->particleID().pid();
    int charge = part->particleID().threeCharge()/3;
    //mcMomentum *= charge;
    Gaudi::XYZPoint mcPosition = part->originVertex()->position();
    double OriginVertexTime = part->originVertex()->time();
    double PrimaryVertexTime = part->primaryVertex()->time();
   
    //// Adding PV information                                                                                                                                                                                                                                                                     
    const LHCb::MCVertex* primary = part->primaryVertex();

    double pvtime= -9999;
    double pvx= -9999;
    double pvy= -9999;
    double pvz= -9999;

    if (primary !=0){
      pvtime = primary->position4vector().t();
      pvx = primary->position4vector().x();
      pvy = primary->position4vector().y();
      pvz = primary->position4vector().z();
    }

    //debug() << " Ignoring tracks created after the Velo "  <<endmsg;

    /*if ( mcPosition.z() > 700. ) {  // ignore tracks created after the Velo.
      continue;
    }

    double eta = 0.5*TMath::Log((mcMomentum+mcMomentum_z)/(mcMomentum-mcMomentum_z));

    if(eta<2.0 or eta>5.0){
      continue;
    }*/



    //if(array_VeloUT_Hist_of_track_Zpos[0]==0.0){
    //  continue;
    //}

    //if(array_Hist_of_track_Zpos[0]==0.0){
    //  continue;
    //}
    //if(array_VeloUT_Hist_of_track_Zpos[0]==0.0){
    //  continue;
    //}

     debug() << " Filling Tuple "  <<endmsg;

    // Fill the ntuple
    Tuple tPoint = nTuple( m_tupleName );
    tPoint->column( "nHits"   , Hits_of_track.size() );
    tPoint->column( "nHits_check"   ,Hist_of_track_Zpos_en.size());
    tPoint->column( "nUTHits"   , flag );

    tPoint->column( "x"   , mcPosition.x() );
    tPoint->column( "y"   , mcPosition.y() );
    tPoint->column( "z"   , mcPosition.z() );
    tPoint->column( "tx"  , mcSlopes.x() );
    tPoint->column( "ty"  , mcSlopes.y() );
    tPoint->column("M"    , Mass);
    tPoint->column("MCParticleID" , ParticleID);    
    tPoint->column( "p"   , mcMomentum );
    tPoint->column( "pz"   , mcMomentum_z );
    tPoint->column( "qop"   , charge/mcMomentum );
    tPoint->column( "OriginVertexTime"   , OriginVertexTime );
    tPoint->column( "PrimaryVertexTime"   , PrimaryVertexTime );
    tPoint->column( "pvtime"   , pvtime );
    tPoint->column( "pvx"   , pvx );
    tPoint->column( "pvy"   , pvy );
    tPoint->column( "pvz"   , pvz );
    //tPoint->column( "HitZpos_0"   , array_Hist_of_track_Zpos[0] );
    //tPoint->column( "HitXpos_0"   , array_Hist_of_track_Xpos[0] );
    //tPoint->column( "HitYpos_0"   , array_Hist_of_track_Ypos[0] );
    //tPoint->column( "HitTpos_0"   , array_Hist_of_track_Tpos[0] );
    tPoint->array( "Hit_entryZ"   , array_Hist_of_track_Zpos_en );
    tPoint->array( "Hit_entryX"   , array_Hist_of_track_Xpos_en );
    tPoint->array( "Hit_entryY"   , array_Hist_of_track_Ypos_en );
    tPoint->array( "Hit_exitZ"   , array_Hist_of_track_Zpos_ex);
    tPoint->array( "Hit_exitX"   , array_Hist_of_track_Xpos_ex);
    tPoint->array( "Hit_exitY"   , array_Hist_of_track_Ypos_ex);
    tPoint->array( "Hit_time"   , array_Hist_of_track_Tpos );

    tPoint->array( "Hit_UT_entryZ"   , array_UT_Hist_of_track_Zpos_en );
    tPoint->array( "Hit_UT_entryX"   , array_UT_Hist_of_track_Xpos_en );
    tPoint->array( "Hit_UT_entryY"   , array_UT_Hist_of_track_Ypos_en );
    tPoint->array( "Hit_UT_exitZ"   , array_UT_Hist_of_track_Zpos_ex);
    tPoint->array( "Hit_UT_exitX"   , array_UT_Hist_of_track_Xpos_ex);
    tPoint->array( "Hit_UT_exitY"   , array_UT_Hist_of_track_Ypos_ex);
    tPoint->array( "Hit_UT_time"   , array_UT_Hist_of_track_Tpos );
    /*tPoint->column( "HitVeloZpos_0"   , array_Velo_Hist_of_track_Zpos[0] );
    tPoint->column( "HitVeloXpos_0"   , array_Velo_Hist_of_track_Xpos[0] );
    tPoint->column( "HitVeloYpos_0"   , array_Velo_Hist_of_track_Ypos[0] );
    tPoint->column( "HitVeloTpos_0"   , array_Velo_Hist_of_track_Tpos[0] );
    tPoint->array( "HitVeloZpos"   , array_Velo_Hist_of_track_Zpos );
    tPoint->array( "HitVeloXpos"   , array_Velo_Hist_of_track_Xpos );
    tPoint->array( "HitVeloYpos"   , array_Velo_Hist_of_track_Ypos );
    tPoint->array( "HitVeloTpos"   , array_Velo_Hist_of_track_Tpos );
    tPoint->column( "HitUTZpos_0"   , array_UT_Hist_of_track_Zpos[0] );
    tPoint->column( "HitUTXpos_0"   , array_UT_Hist_of_track_Xpos[0] );
    tPoint->column( "HitUTYpos_0"   , array_UT_Hist_of_track_Ypos[0] );
    tPoint->column( "HitUTTpos_0"   , array_UT_Hist_of_track_Tpos[0] );
    tPoint->array( "HitUTZpos"   , array_UT_Hist_of_track_Zpos );
    tPoint->array( "HitUTXpos"   , array_UT_Hist_of_track_Xpos );
    tPoint->array( "HitUTYpos"   , array_UT_Hist_of_track_Ypos );
    tPoint->array( "HitUTTpos"   , array_UT_Hist_of_track_Tpos );
    tPoint->column( "HitVeloUTZpos_0"   , array_VeloUT_Hist_of_track_Zpos[0] );
    tPoint->column( "HitVeloUTXpos_0"   , array_VeloUT_Hist_of_track_Xpos[0] );
    tPoint->column( "HitVeloUTYpos_0"   , array_VeloUT_Hist_of_track_Ypos[0] );
    tPoint->column( "HitVeloUTTpos_0"   , array_VeloUT_Hist_of_track_Tpos[0] );
    tPoint->array( "HitVeloUTZpos"   , array_VeloUT_Hist_of_track_Zpos );
    tPoint->array( "HitVeloUTXpos"   , array_VeloUT_Hist_of_track_Xpos );
    tPoint->array( "HitVeloUTYpos"   , array_VeloUT_Hist_of_track_Ypos );
    tPoint->array( "HitVeloUTTpos"   , array_VeloUT_Hist_of_track_Tpos );*/
    tPoint->column( "eventNumber"   , odin->eventNumber() );
    tPoint->column( "PathLength"   , TotalPathLength );
    tPoint->column( "PathLengthExample"   , PathLengthExample );
    tPoint->write();

    debug() << " DONE Filling Tuple "  <<endmsg;

  }

  if( msgLevel(MSG::DEBUG) ) debug() << "Linker table stored at " << m_outputData << endmsg;


  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode MCParticleNTuple::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}

//=============================================================================

//  LocalWords:  endmsg
