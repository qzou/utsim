// Include files

#include "Event/Track.h"
#include "Event/MCParticle.h"
#include "Event/MCHit.h"
#include "Event/STCluster.h"
#include "Event/ODIN.h"

// from Event/LinkerEvent
#include "Linker/LinkedTo.h"
#include "Linker/LinkerWithKey.h"
#include "Linker/LinkedFrom.h"

// local
#include "UtCluster.h"

#include "TMath.h"


//-----------------------------------------------------------------------------
// Implementation file for class : UtCluster
//
// 2020-05-01 : Quan (starting from Olivier Callot PatCheckerNTuple)
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UtCluster )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UtCluster::UtCluster( const std::string& name,
                                    ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator )
{
  declareProperty( "NtupleName"           , m_tupleName = "UtClusters"    );
  declareProperty("Detectors", m_dets ={"UT"} );//{"VP", "UT", "FT"}
  //declareProperty("Detectors", m_dets = {"FT"});
  //declareProperty( "OutputData",  m_outputData = LHCb::MCParticleLocation::Default + "2MCHits" );
  //declareProperty( "MCHitPath",   m_inputData );
  //declareProperty( "OutputData",  m_outputData );
  //  declareProperty( "InputTracks"          , m_container = LHCb::TrackLocation::Forward );
}
//=============================================================================
// Destructor
//=============================================================================
UtCluster::~UtCluster() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode UtCluster::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode UtCluster::execute() {

  debug() << "==> Execute" << endmsg;
  const LHCb::STCluster::Container* utClusters = get<LHCb::STCluster::Container>(LHCb::STClusterLocation::UTClusters);//(LHCb::STClusterLocation::UTClusters);
  // get MCHits UT
  //LHCb::MCHits* mcUTHits = get<LHCb::MCHits>(  "/Event/MC/UT/Hits" );

  // warning() << "Linker table stored at " << m_outputData << endmsg;
  // save some typing

  //LinkedFrom<LHCb::MCHit,LHCb::MCParticle> myMCHitLink( evtSvc(), msgSvc(), LHCb::MCParticleLocation::Default + "2MC" + "UT" + "Hits" );
  LinkedTo<LHCb::MCParticle> myClusterLink ( evtSvc(), msgSvc(), m_UTcluster2ParticleLinkerLocation );
  LinkedTo<LHCb::MCHit> myUTCluster2MCHitLink ( evtSvc(),msgSvc(), m_UTcluster2MCHitsLinkerLocatin);

  for ( LHCb::STCluster::Container::const_iterator itT = utClusters->begin(); utClusters->end() != itT ; ++itT ) {
    LHCb::STCluster* Cluster = *itT;

    //std::vector< LHCb::UTChannelID* > channels_of_cluster;
    std::vector<unsigned int>DetType;
    std::vector<unsigned int>Station;
    std::vector<unsigned int>Layer;
    std::vector<unsigned int>Region;
    std::vector<unsigned int>Sector;
    std::vector<unsigned int>Strip;
    std::vector<unsigned int>uniqueLayer;

    std::vector<std::pair<int, unsigned int> > Strips;
    Strips=Cluster->stripValues();
    std::vector<int> StripNums;
    std::vector<unsigned int> StripValues;
    stripSize=Strips.size();

    //FOR DEBUG AND TEST
    doubel Link_use_Channel=Cluster->channelID();
    
    for(unsigned int i=0;i<Strips.size();i++){
      StripNums.push_back(Strips[i].first);
      StripValues.push_back(Strips[i].second);
     }
    //fot Linked MCHits
    std::vector<float>Hits_of_cluster_Zpos;
    std::vector<float>Hits_of_cluster_Xpos;
    std::vector<float>Hits_of_cluster_Ypos;
    std::vector<float>Hits_of_cluster_Tpos;
    std::vector<float>Hits_of_cluster_E;
   
    //for Linked McParticles
    std::vector<double>MCpart_of_cluster_P;
    std::vector<double>MCpart_of_cluster_Pz;
    std::vector<int>MCpart_of_cluster_charge;
    std::vector<double>MCpart_of_cluster_id;
    std::vector<double>MCpart_of_cluster_E;

    std::vector<double>MCpart_of_cluster_OriginPosition;
    std::vector<double>MCpart_of_cluster_PVposition;//(x,y,z,t)
    //std::vector<double>Num_of_PV_of_particle;

    //access linker from Here
    /*LHCb::MCParticle* mcpart = myClusterLink.first(Cluster->channelID());//hit->id().ftID() key for the keyed object
    if(mcpart==nullptr) warning()<<"This Cluster doesn't have a related MCParticle"<<endmsg;
    else{
      while(mcpart!=nullptr){
      //Gaudi::XYZVector mcSlopes  = part->momentum().Vect();
      //mcSlopes /= mcSlopes.z();
      MCpart_of_cluster_P.push_back( mcpart->momentum().P() );
      MCpart_of_cluster_Pz.push_back( mcpart->momentum().Pz() );
      MCpart_of_cluster_charge.push_back (mcpart->particleID().threeCharge()/3);
      MCpart_of_cluster_id.push_back(mcpart->particleID().pid());
      MCpart_of_cluster_E.push_back(mcpart->momentum().E());
      //mcMomentum *= charge;
      MCpart_of_cluster_OriginPosition.push_back(mcpart->originVertex()->position().x());
      MCpart_of_cluster_OriginPosition.push_back(mcpart->originVertex()->position().y());
      MCpart_of_cluster_OriginPosition.push_back(mcpart->originVertex()->position().z());
      MCpart_of_cluster_OriginPosition.push_back(mcpart->originVertex()->time());

      //double PrimaryVertexTime = mcpart->primaryVertex()->time();

      // Adding PV information
      const LHCb::MCVertex* primary = mcpart->primaryVertex();
      double pvtime= -9999;
      double pvx= -9999;
      double pvy= -9999;
      double pvz= -9999;

      if (primary !=0){
        pvtime = primary->position4vector().t();
        pvx = primary->position4vector().x();
        pvy = primary->position4vector().y();
        pvz = primary->position4vector().z();
        }
      MCpart_of_cluster_PVposition.push_back(pvx);
      MCpart_of_cluster_PVposition.push_back(pvy);
      MCpart_of_cluster_PVposition.push_back(pvz);
      MCpart_of_cluster_PVposition.push_back(pvtime);

      mcpart=myClusterLink.next();
    }
   }
     int Nparticles=MCpart_of_cluster_id.size();
*/
    //LHCb::MCParticle* mcPart1 = myClusterLink.first(cluster->channelID());//hit->id().ftID() key for the keyed object
   // std::vector <int> Nhits_cluster;
    LHCb::MCHit* mcHit = myUTCluster2MCHitLink.first( Cluster->channelID());
    if( mcHit==nullptr){
        warning()<<"This Cluster doesn't have related Hits"<<endmsg;
     }
    else{
      while ( mcHit!= nullptr ) {

        Hits_of_cluster_Xpos.push_back( mcHit->entry().x() );
        Hits_of_cluster_Ypos.push_back( mcHit->entry().y() );
        Hits_of_cluster_Zpos.push_back( mcHit->entry().z() );
        Hits_of_cluster_Tpos.push_back( mcHit->time());
        Hits_of_cluster_E.push_back(mcHit->energy());
        mcHit = myUTCluster2MCHitLink.next();
      }
    }

    int Nhits=Hits_of_cluster_Xpos.size();

    std::vector< LHCb::STChannelID> channels=Cluster->channels();
    if(channels.size()==0)return StatusCode::FAILURE;

    for( std::vector< LHCb::STChannelID>::iterator itCh=channels.begin();channels.end() != itCh;++itCh){
         LHCb::STChannelID channel=*itCh;
         //channels_of_cluster.push_back(channel);
         //DetType.push_back(channel.DetType());
         Station.push_back(channel.station());
         Layer.push_back(channel.layer());
         Region.push_back(channel.detRegion());
         Sector.push_back(channel.sector());
         Strip.push_back(channel.strip());
         uniqueLayer.push_back(channel.uniqueLayer());
    }

    float ClusterADC=Cluster->totalCharge();
    int ClusterSize=Cluster->size();
    //int vectorSize=Station.size();


    const LHCb::ODIN* odin = getIfExists<LHCb::ODIN>(LHCb::ODINLocation::Default);
    debug() << " Filling Tuple "  <<endmsg;

    // Fill the ntuple
    Tuple tPoint = nTuple( m_tupleName );
    tPoint->column( "nClusters"   , ClusterSize );
    tPoint->column( "nHits"   , Nhits );
    tPoint->column( "ADC"   , ClusterADC );
   // tPoint->column(" nParticles"   , Nparticles );
    
    tPoint->column( "stripSize"   , stripSize );
    tPoint->column( "channel_linked"   , Link_use_Channel );

    tPoint->array( "DetType"   , DetType );
    tPoint->array( "Station"   , Station );
    tPoint->array( "Region"   ,  Region);
    tPoint->array( "Sector"   , Sector);
    tPoint->array( "Strip"   , Strip );
    tPoint->array( "uniqueLayer"   ,uniqueLayer );
    
    tPoint->array( "stripNums"   ,StripNums );
    tPoint->array( "stripValues"   ,StripValues );
/*
    tPoint->array( "array_MCPart_E_Cluster"   , MCpart_of_cluster_E );
    tPoint->array( "array_MCPart_P_Cluster"   , MCpart_of_cluster_P );
    tPoint->array( "array_MCPart_Pz_Cluster"   , MCpart_of_cluster_Pz );
    tPoint->array( "array_MCPart_charge_Cluster"   , MCpart_of_cluster_charge );
    tPoint->array( "array_MCPart_Pid_Cluster"   , MCpart_of_cluster_id );
    tPoint->array( "array_MCPart_OriVectex_Cluster"   , MCpart_of_cluster_OriginPosition );
    tPoint->array( "array_MCPart_OwnPV_Cluster"   , MCpart_of_cluster_PVposition );
*/
    tPoint->array( "array_HitXpos_Cluster"   , Hits_of_cluster_Xpos );
    tPoint->array( "array_HitYpos_Cluster"   , Hits_of_cluster_Ypos );
    tPoint->array( "array_HitZos_Cluster"   , Hits_of_cluster_Zpos );
    tPoint->array( "array_HitTpos_Cluster"   , Hits_of_cluster_Tpos );
    tPoint->array( "array_HitE_Cluster"   , Hits_of_cluster_E );

    tPoint->column( "eventNumber"   , odin->eventNumber() );
    tPoint->write();

    debug() << " DONE Filling Tuple "  <<endmsg;

  }

  if( msgLevel(MSG::DEBUG) ) debug() << "Linker table stored at " << m_outputData << endmsg;
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode UtCluster::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}

//=============================================================================

//  LocalWords:  endmsg
