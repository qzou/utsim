from Gaudi.Configuration import *
#Auto generated by mkjob.sh
from Configurables import Boole
#from Gaudi.Configuration import importOptions
#from Boole.Configuration import * 

############################################################################                                                                                        
# File for running Boole with default settings (full Digi output)                                                                                                   
#                                                                                                                                                                   
# Syntax is:                                                                                                                                                        
# gaudirun.py Boole/Default.py Conditions/<someTag>.py <someDataFiles>.py                                                                                           
############################################################################                                                                                        
from Configurables import Boole, CondDB
from Boole.Configuration import *
                                                                            
Boole().UseSpillover = False
#Boole().SpilloverPaths = ["PrevPrev", "Prev", "Next"]
Boole().DataType  = "Upgrade"
Boole().Outputs = [ "DIGI" ]
Boole().DigiType = "Extended"
#Boole().DetectorDigi = ['VP','UT','FT']
#Boole().DetectorLink = ['VP','UT','FT']
#Boole().DetectorMoni = ['VP','UT','FT']
Boole().DetectorDigi = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt']
Boole().DetectorLink = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Tr']
Boole().DetectorMoni = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Tr', 'MC']

Boole().InputDataType = "SIM"

CondDB().Upgrade = True

#tags from Laurent
Boole().DDDBtag   = "dddb-20171010"
Boole().CondDBtag = "sim-20180530-vc-md100" #"sim-20170301-vc-md100"

#Boole().DDDBtag = "dddb-20170726"
#Boole().CondDBtag = "sim-20170301-vc-mu100"

#tags and option from Upgrade MC central productions - FT geom 6.3 too new and too many detectors
#Boole().DDDBtag = "dddb-20171126"
#Boole().CondDBtag = "sim-20171127-vc-md100"
#importOptions("$APPCONFIGOPTS/Boole/Default.py")
#importOptions("$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20150522.py")
#importOptions("$APPCONFIGOPTS/Boole/EnableSpillover.py")
#importOptions("$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py") 

from Configurables import OutputStream
#EventSelector().Input = ["DATAFILE='PFN:/eos/lhcb/user/l/lgrillo/Gauss-30000000_hiLumi_production_nospill.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
#EventSelector().Input = ["DATAFILE='PFN:/eos/lhcb/user/l/ldufour/MightyTracker/SimulationFiles/October2018/2e34/minibias/minibias_MagUp_1_Gauss.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
EventSelector().Input                = ["DATAFILE='/publicfs/lhcb/user/zouquan/Sim/100events/Gauss/tuple/Gauss_3.sim ' TYP='POOL_ROOTTREE' OPT='READ'"]
OutputStream("DigiWriter").Output    =  "DATAFILE='PFN:/publicfs/lhcb/user/zouquan/Sim/100events/Boole/tuple/Boole_3.digi' TYP='POOL_ROOTTREE' OPT='RECREATE'"
HistogramPersistencySvc().OutputFile = "/publicfs/lhcb/user/zouquan/Sim/100events/Boole/tuple/monitor/Boole_monitor_3.root"

############################################################################                                                                                        
