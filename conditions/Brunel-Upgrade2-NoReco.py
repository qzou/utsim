###############################################################################
# File for running alignment with D0 reconstructed from HLT sel reports
###############################################################################
# Syntax is:
#   gaudirun.py Brunel-testMC-simple.py <someDataFiles>.py
###############################################################################

import os, sys
#os.environ['NO_GIT_CONDDB'] = '1'

from Configurables import Brunel, LHCbApp, CondDB, NTupleSvc, DigiConf
from Gaudi.Configuration import *

# Just instantiate the configurable...
theApp = Brunel()
theApp.DataType   = "Upgrade"
theApp.InputType  = "DIGI"
DigiConf().DigiType = "Extended"
theApp.WithMC = True
theApp.Simulation = True
#theApp.Detectors  = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Tr' ]
#theApp.RecoSequence = ["Decoding","TrFast","TrBest","RICH","CALO","MUON","PROTO","SUMMARY"]
theApp.Detectors  = ['VP', 'UT', 'FT', 'Magnet', 'Tr' ]
#theApp.RecoSequence = ["Decoding","TrFast", "TrBest", "PROTO"]
#theApp.Detectors  = []
theApp.RecoSequence = []
theApp.Monitors = []

theApp.PrintFreq = 10
#theApp.EvtMax = 50
theApp.EvtMax = -1
theApp.MCLinksSequence = ["Unpack", "Tr"] #LDST?
theApp.OutputType = 'ROOT'
#theApp.SplitRawEventInput = 4.1
#theApp.SplitRawEventOutput = 4.1
#theApp.DatasetName = 'MC-reso2017-SigMC-NoGP'
theApp.DatasetName = 'MChists'

#theApp.DDDBtag   = "dddb-20150724"
#theApp.CondDBtag   = "sim-20160606-vc-md100" 

#theApp.DDDBtag   = "dddb-20150724"
#theApp.CondDBtag   = "sim-20161124-2-vc-md100"

# Tags for Brunel v52r6p1 - FT Geom 61 needed
#theApp.DDDBtag   = "dddb-20170301"
#theApp.CondDBtag   = "sim-20170301-vc-mu100"

# Tags for Brunel v49r0
#theApp.DDDBtag   = "dddb-20150729"
#theApp.CondDBtag   = "sim-20150716-vc-mu100"

# Tags for Brunel v51r0 from Renato
CondDB().Upgrade = True
#For Yunlong
#theApp.DDDBtag    = "dddb-20150424" # Upgrade database tags
#theApp.CondDBtag  = "sim-20140204-vc-md100"
#Laurent
#Boole().DDDBtag   = "dddb-20171010"
#Boole().CondDBtag = "sim-20180530-vc-md100"
theApp.DDDBtag = "dddb-20171010"#"dddb-20170726"
theApp.CondDBtag = "sim-20170301-vc-md100"


#from Configurables import MessageSvc
#MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#from Configurables import DstConf
#DstConf(EnableUnpack=["Reconstruction", "Stripping"])

#from Configurables import DecodeRawEvent
#DecodeRawEvent().setProp("OverrideInputs",4.1)
from Configurables import CondDBAccessSvc, CondDB, UpdateManagerSvc, L0Conf, OutputStream, NTupleSvc


from Gaudi.Configuration import appendPostConfigAction
CondDB().UseOracle = False
CondDB().IgnoreHeartBeat= True
CondDB().EnableRunStampCheck = False
#CondDB().LocalTags["SIMCOND"] = ["ot-20160722"]

L0Conf().EnsureKnownTCK=False


from Configurables import GaudiSequencer, MCParticleNTuple, MCVerticesTuple


def setup_mc_truth_matching():
    GaudiSequencer("CheckPatSeq").Members = [ MCParticleNTuple("MCParticleNTuple") ]
    GaudiSequencer("CheckPatSeq").Members += [ MCVerticesTuple("MCVerticesTuple") ]
    NTupleSvc().Output = [ "FILE1 DATAFILE='MCtracks.root' TYP='ROOT' OPT='NEW'" ]
    GaudiSequencer('MoniTrSeq').Members = []
    GaudiSequencer('MCLinksTrSeq').Members = []
#    GaudiSequencer("CheckPatSeq").Members = [ MCVerticesTuple("MCVerticesTuple") ]
#    NTupleSvc().Output = [ "FILE1 DATAFILE='MCVertices.root' TYP='ROOT' OPT='NEW'" ]
#    GaudiSequencer('MoniTrSeq').Members = []
#    GaudiSequencer('MCLinksTrSeq').Members = []

#    GaudiSequencer("CheckPatSeq").Members += [ PrChecker("PrChecker") ]
#    PrChecker().Upgrade = False
#    PrChecker().WriteVeloHistos = 1
#    PrChecker().WriteForwardHistos = 1
#    PrChecker().WriteMatchHistos = 1
#    PrChecker().WriteDownHistos = 1
#    PrChecker().WriteUpHistos = 1
#    PrChecker().WriteTTrackHistos = 1
#    PrChecker().WriteBestHistos = 1
#    PrChecker().WriteBestLongHistos = 1
#    PrChecker().WriteBestDownstreamHistos = 1
#    #PrChecker().GhostProbCut = 0.4
#    PrChecker().WriteUTHistos = 1
##    #GaudiSequencer("CheckPatSeq").Members += [ PrClustersResidual("PrClustersResidual") ]
#    
#    #TrackEffChecker("TrackEffChecker").addTool( IdealStateCreator("IdealStateCreator"))
#    #TrackEffChecker("TrackEffChecker").IdealStateCreator.Extrapolator = "TrackMasterExtrapolator"
#    #TrackEffChecker("TrackEffChecker").RequireLongTrack = True
#    #GaudiSequencer("CheckPatSeq").Members += [ TrackEffChecker("TrackEffChecker") ]
    

appendPostConfigAction(setup_mc_truth_matching)

OutputStream("DstWriter").ItemList

#from GaudiConf import IOHelper                                                                                                                                                                         
#IOHelper().inputFiles([  
#        '/afs/cern.ch/user/l/lgrillo/cmtuser/Gauss_v50r0/scripts/Boole_test.xdigi'
#         ], clear=True)
#

#Digi files in input
Filedir="/publicfs/lhcb/user/zouquan/Sim/100events/Boole_run3/tuple/"
from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
    #Differnt beam conditions
	'/publicfs/lhcb/user/zouquan/Sim/100events/Boole_run3/tuple/Boole_1.digi',
	Filedir+'Boole_2.digi',    
	Filedir+'Boole_3.digi',
  	Filedir+'Boole_4.digi',
	Filedir+'Boole_5.digi',
	Filedir+'Boole_6.digi',
	Filedir+'Boole_7.digi',
	Filedir+'Boole_8.digi',
	Filedir+'Boole_9.digi',
	Filedir+'Boole_10.digi'
#'/afs/cern.ch/work/l/lgrillo/MightyIT/MightyMatchingTiming/MightyIT/DigiGeneration/Boole_MagUp_13104012.xdigi'
], clear=True)
#FileCatalog().Catalogs += [ 'xmlcatalog_file:RICHlessPFN.xml' ]
