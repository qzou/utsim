############################################################################                                                                                        
# File for running Boole with default settings (full Digi output)                                                                                                   
#                                                                                                                                                                   
# Syntax is:                                                                                                                                                        
# gaudirun.py Boole/Default.py Conditions/<someTag>.py <someDataFiles>.py                                                                                           
############################################################################                                                                                        
from Configurables import Boole, CondDB
from Boole.Configuration import *
                                                                            
Boole().UseSpillover = True
Boole().SpilloverPaths = ["PrevPrev", "Prev", "Next", "NextNext"]
Boole().DataType  = "Upgrade"
Boole().Outputs = [ "DIGI" ]
Boole().DigiType = "Extended"
#Boole().DetectorDigi = ['VP','UT','FT']
#Boole().DetectorLink = ['VP','UT','FT']
#Boole().DetectorMoni = ['VP','UT','FT']
Boole().DetectorDigi =['UT','FT','VP']# ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt']
Boole().DetectorLink = []#['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Tr']
Boole().DetectorMoni = []#['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Tr', 'MC']

Boole().InputDataType = "SIM"

CondDB().Upgrade = True

#tags from Laurent
#Boole().DDDBtag = "dddb-20170726"
#Boole().CondDBtag = "sim-20170301-vc-mu100"

#Latest (20.01.2020) Upgrade (1) Tags
#LHCbApp().DDDBtag = "dddb-20190223"
#LHCbApp().CondDBtag = "sim-20180530-vc-mu100"

LHCbApp().DDDBtag = "dddb-20201105"
LHCbApp().CondDBtag = "sim-20201030-vc-mu100"
#tags and option from Upgrade MC central productions - FT geom 6.3 too new and too many detectors
#Boole().DDDBtag = "dddb-20171126"
#Boole().CondDBtag = "sim-20171127-vc-md100"
#importOptions("$APPCONFIGOPTS/Boole/Default.py")
#importOptions("$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20150522.py")
#importOptions("$APPCONFIGOPTS/Boole/EnableSpillover.py")
#importOptions("$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py") 

#add read sequence
from Configurables import GaudiSequencer,MCParticleNTuple,OutputStream, NTupleSvc#, MCVerticesTuple
def mySequencer():
  GaudiSequencer("DigiUTSeq").Members += [ MCParticleNTuple("MCParticleNTuple") ]
  NTupleSvc().Output = [ "FILE1 DATAFILE='./Boole_outputMChits.root' TYP='ROOT' OPT='NEW'"]

appendPostConfigAction(mySequencer)
'''
input=[]
txt='./scripts/job1_LFNs.txt'
with open(txt,'r') as f:
  lines=f.readlines()
  for l in lines:
    line="DATAFILE='PFN:/eos/lhcb/grid/user"+l.split('\n')[0]+'\''
    location=line+" TYP='POOL_ROOTTREE' OPT='READ'"
    input.append(location)
print (len(input))
'''
from Configurables import OutputStream
#OutputStream("DigiWriter").Output = 'Boole_MagUp_30000000.xdigi'
#in ganga dont need this input

#EventSelector().Input =input # ["DATAFILE='PFN:/afs/cern.ch/user/q/qzou/eos/qzou/utSim/20evt/test/hiLumi_production_nospill_ev10.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
#We dont do real digi so this is spare
#OutputStream("DigiWriter").Output = 'PFN:./Boole_MagUp_30000000_3evt.xdigi'
############################################################################                                                                                        

